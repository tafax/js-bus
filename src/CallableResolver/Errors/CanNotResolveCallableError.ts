
import { CallableResolverError } from './CallableResolverError';

export class CanNotResolveCallableResolverError extends CallableResolverError {}
