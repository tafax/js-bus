
import { CallableResolverError } from './CallableResolverError';

export class UndefinedHandleCallableError extends CallableResolverError {}
