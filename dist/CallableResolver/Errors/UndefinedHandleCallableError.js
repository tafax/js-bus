"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CallableResolverError_1 = require('./CallableResolverError');
var UndefinedHandleCallableError = (function (_super) {
    __extends(UndefinedHandleCallableError, _super);
    function UndefinedHandleCallableError() {
        _super.apply(this, arguments);
    }
    return UndefinedHandleCallableError;
}(CallableResolverError_1.CallableResolverError));
exports.UndefinedHandleCallableError = UndefinedHandleCallableError;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9DYWxsYWJsZVJlc29sdmVyL0Vycm9ycy9VbmRlZmluZWRIYW5kbGVDYWxsYWJsZUVycm9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBLHNDQUFzQyx5QkFBeUIsQ0FBQyxDQUFBO0FBRWhFO0lBQWtELGdEQUFxQjtJQUF2RTtRQUFrRCw4QkFBcUI7SUFBRSxDQUFDO0lBQUQsbUNBQUM7QUFBRCxDQUF6RSxBQUEwRSxDQUF4Qiw2Q0FBcUIsR0FBRztBQUE3RCxvQ0FBNEIsK0JBQWlDLENBQUEiLCJmaWxlIjoiQ2FsbGFibGVSZXNvbHZlci9FcnJvcnMvVW5kZWZpbmVkSGFuZGxlQ2FsbGFibGVFcnJvci5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IHsgQ2FsbGFibGVSZXNvbHZlckVycm9yIH0gZnJvbSAnLi9DYWxsYWJsZVJlc29sdmVyRXJyb3InO1xuXG5leHBvcnQgY2xhc3MgVW5kZWZpbmVkSGFuZGxlQ2FsbGFibGVFcnJvciBleHRlbmRzIENhbGxhYmxlUmVzb2x2ZXJFcnJvciB7fVxuIl19
