import { CallableResolverError } from './CallableResolverError';
export declare class CanNotResolveCallableResolverError extends CallableResolverError {
}
