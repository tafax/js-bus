"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CallableResolverError_1 = require('./CallableResolverError');
var CanNotResolveCallableResolverError = (function (_super) {
    __extends(CanNotResolveCallableResolverError, _super);
    function CanNotResolveCallableResolverError() {
        _super.apply(this, arguments);
    }
    return CanNotResolveCallableResolverError;
}(CallableResolverError_1.CallableResolverError));
exports.CanNotResolveCallableResolverError = CanNotResolveCallableResolverError;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9DYWxsYWJsZVJlc29sdmVyL0Vycm9ycy9DYW5Ob3RSZXNvbHZlQ2FsbGFibGVFcnJvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxzQ0FBc0MseUJBQXlCLENBQUMsQ0FBQTtBQUVoRTtJQUF3RCxzREFBcUI7SUFBN0U7UUFBd0QsOEJBQXFCO0lBQUUsQ0FBQztJQUFELHlDQUFDO0FBQUQsQ0FBL0UsQUFBZ0YsQ0FBeEIsNkNBQXFCLEdBQUc7QUFBbkUsMENBQWtDLHFDQUFpQyxDQUFBIiwiZmlsZSI6IkNhbGxhYmxlUmVzb2x2ZXIvRXJyb3JzL0Nhbk5vdFJlc29sdmVDYWxsYWJsZUVycm9yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgeyBDYWxsYWJsZVJlc29sdmVyRXJyb3IgfSBmcm9tICcuL0NhbGxhYmxlUmVzb2x2ZXJFcnJvcic7XG5cbmV4cG9ydCBjbGFzcyBDYW5Ob3RSZXNvbHZlQ2FsbGFibGVSZXNvbHZlckVycm9yIGV4dGVuZHMgQ2FsbGFibGVSZXNvbHZlckVycm9yIHt9XG4iXX0=
