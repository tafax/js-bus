import { CallableResolverError } from './CallableResolverError';
export declare class UndefinedHandleCallableError extends CallableResolverError {
}
